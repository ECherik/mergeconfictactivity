package geometry;

public class Square {
   private double base, height;
   public Square(double base,double height){
    this.base = base;
    this.height =height;
   } 
   public double getBase(){
      return base;
   }
   public double getHeight() {
       return height;
   }
   public double getArea(){
    return base*height;
   }
   public String toString(){
      return "base is: "+base+" height is: "+height;
   }
}
