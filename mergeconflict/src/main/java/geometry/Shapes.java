package geometry;

public class Shapes {
    public static void main(String[]args){
        Circle newCircle = new Circle(4);
        Square square = new Square(4, 10);
        
        System.out.println(square+" the area of the square is: "+square.getArea());
        System.out.println(newCircle.getArea());
        System.out.println(newCircle);
    }
}
