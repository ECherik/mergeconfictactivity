package geometry;

public class Circle {
    private double radius;

    public Circle(double newRadius){
        this.radius = newRadius;
    }

    public double getRadius(){
        return this.radius;
    }

    public double getArea(){
        double result = Math.PI * (Math.pow(radius,2));
        return result;
    }

    public String toString(){
        return "This circle has a radius of " + this.radius + " " + "and an area of " + getArea() + ".";
    }
}
