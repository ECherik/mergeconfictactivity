package geometry;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class SquareTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void testGetMethod()
    {
        Square square = new Square(4,10);
        assertTrue( square.getBase()==4);
        assertTrue( square.getHeight()==10);
    }
    @Test
    public void testGetArea()
    {
        Square square = new Square(4,10);
        assertTrue(square.getArea()==40);
    }
    @Test
    public void testToString()
    {
        Square square = new Square(4,10);
        assertEquals("base is: 4.0 height is: 10.0", square.toString());
    }
}
