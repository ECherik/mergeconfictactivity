package geometry;

import static org.junit.Assert.*;

import org.junit.Test;

public class CircleTest 
{
    final double TOLERANCE = 0.000000001;
    @Test
    public void getMethodsTest()
    {
        Circle newCircle = new Circle(4);
        assertEquals(4, newCircle.getRadius(), TOLERANCE);
    }
    @Test
    public void getAreaTest(){
        Circle newCircle = new Circle(4);
        assertEquals(50.2654824574, newCircle.getArea(), TOLERANCE);
    }
    @Test
    public void toStringTest(){
        Circle newCircle = new Circle(4);
           assertEquals( "This circle has a radius of 4.0 and an area of 50.26548245743669.", newCircle.toString());
    } 

}
